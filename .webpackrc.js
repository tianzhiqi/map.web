const path = require('path');

export default {
  entry: 'src/index.js',
  extraBabelPlugins: [['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }]],
  env: {
    development: {
      extraBabelPlugins: ['dva-hmr'],
    },
  },
  html: {
    template: 'src/index.ejs',
  },
  disableDynamicImport: true,
  publicPath: '/',
  hash: true,
  proxy: {
    '/manage': {
      target: 'http://120.26.169.89:8088',
      changeOrigin: true,
      //pathRewrite: { '^/api': '' },
    },
  },
};
