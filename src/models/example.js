import { query, queryByStatus } from '../services/example'

export default {

  namespace: 'example',

  state: {
    poslist: {},
    total: {},
    subData: {},
    filterData: {},
    handelItem:{},
    echartsData:{},
    ItemsDevelopment:{},
    ItemDetails:{},
    DevelopmentSituation:[],
    tableList: []
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {  // eslint-disable-line
      const res = yield call(query, payload)
      yield put({
        type: 'save',
        payload: {
          poslist: res
        }
      });
    },
    *fetchNext({ payload }, { call, put }) {
      const res = yield call(query, payload)
      yield put({
        type: 'save',
        payload: {
          subData: res.data
        }
      });
    },
    *fetchTotal({ payload }, { call, put }) {
      const res = yield call(query, payload)
      yield put({
        type: 'save',
        payload: {
          total: res
        }
      })
    },
    *fetchByStatus({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          total: res
        }
      })
    },
    *fetchByProperty({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          total: res
        }
      })
    },
    *fetchByFormat({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          total: res
        }
      })
    },
    *fetchByNature({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          total: res
        }
      })
    },
    *fetchByEcharts({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          echartsData: res
        }
      })
    },
    *fetchByEchartsArea({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          echartsData: res
        }
      })
    },
    *fetchByEchartsCondition({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          echartsData: res
        }
      })
    },
    *fetchByEchartsNature({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          echartsData: res
        }
      })
    },
    *fetchByItemsDevelopment({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          ItemsDevelopment: res
        }
      })
    },
    *fetchByItemsDetails({ payload }, { call, put }) {
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          ItemDetails: res.data
        }
      })
    },
    *fetchDevelopmentSituation({ payload }, { call, put }){
      const res = yield call(queryByStatus, payload)
      yield put({
        type: 'save',
        payload: {
          DevelopmentSituation: res.data.rows
        }
      })
    },
    *fetchPage({ payload }, { call, put }) {
      const res = yield call(query, payload)
      yield put({
        type: 'save',
        payload: {
          tableList: res.data.rows
        }
      })
    },
    *fetchImg({ payload }, { call, put}){
      const res = yield call(query, payload)
      yield put({
        type: 'save',
        payload: {
          imgList: res
        }
      })
    }

  },


  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
  },

};
