import request from '../utils/request';
import { stringify } from 'qs'

export function query(params) {
  return request(`/manager/ajax.html?${stringify(params)}`);
}
export function queryByStatus(params) {
  return request(`/manager/ajax.html?${stringify(params)}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
    },
    body: stringify(params)
  })
}