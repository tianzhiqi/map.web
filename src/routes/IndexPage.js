import React from 'react';
import { connect } from 'dva';
import styles from './IndexPage.css';
import { Map, Polygon, InfoWindow,Marker } from 'react-amap';
import { Checkbox ,Table,Carousel} from 'antd';
import ReactEcharts from 'echarts-for-react';
const itemPropertyList = ['观光', '度假', '专项'];
const itemTypeList = ['通用航空（低空）旅游','旅游装备制造','旅游车船制造','纪念馆',
'历史古迹','探险','露营','潜水','登山','古村落','特色餐饮','滑雪','旅游培训',
'基础设施','电子商务','集散咨询','会议会展','文化演出','商贸购物','高尔夫','自助营地',
'温泉养生','博物馆','主题公园','特色街区','旅游村镇','人文景观','自然景区','宾馆酒店','大型综合','邮轮游艇','其他'];
const imItemList = ['新建','改建','扩建','改（扩）建','恢复','单纯购置'];
const formatList = ['文化旅游','生态旅游','海洋旅游4','医疗健康旅游5',
'乡村旅游','商务旅游','红色旅游','体育旅游','工业旅游','森林旅游','邮轮游艇旅游','航空旅游'];
const stateList = ['项目前期','项目设计期','项目工程期','项目营销期','项目运营管理期'];

const projectPosition = ['城区', '乡村'];

@connect(({example}) => ({
  example
}))
export default class IndexPage extends React.Component {
  state = {
    mapKey: '893febe375679b9a415b19c7c52d206e',
    zoom: 8.5,
    visible: false,
    opacity: 1,
    size: {
      width: 500,
      height: 190,
    },
    offset: [0, 0],
    position: {
      longitude: 103.766196, // 泸西
      latitude: 24.532025
    },
    title: '',
    projectList: [],
    subPos: [],
    isSub: false,
    isImg: true,
    selectedImg: '',
    subPosition: {},
    center: {
      longitude: 102.826557, // 建水县
      latitude: 23.6347
    },
    isConstruct: false,
    isShowMenu:false,
    isItemTotal:false,
    tablename :'',
    construct: null,
    tablename:'',
    tablenameTitle:'',
    itemxms :[],
    itemcountyname:[],
    isViewArea:false,
    isViewTotal:false,
    isViewCondition:false,
    isViewNature:false,
    isShowItemDetail:false,
    isShowItemDevelopment:false,
    windowHeight:0,
    riqiColor:'',
    selectedQxbm: ''
  }

  //进入页面加载标记点
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'example/fetchTotal',
      payload: {
        ajax: 'lvxmXR'
      }
    })
  }
    //根据建设状态筛选
    getConstruct = (e) =>{
      const condtion=e.join()
      const qq = encodeURI(condtion)
      this.setState({
        constructPraram:qq
      })
      const { dispatch } = this.props;
      const {propertyParam,formatParam,natureParam} =this.state
      dispatch({
        type: 'example/fetchByStatus',
        payload: {
          ajax: 'lvxmXR',
          jszt:qq,//建设状态
          xmsx:propertyParam,//项目属性
          xmyt:formatParam,//项目业态
          xmxz:natureParam,//项目性质
        }
      })
    }

    //根据项目属性筛选
    getProperty = (e) =>{
      const condtion=e.join()
      const qq = encodeURI(condtion)
      const { dispatch } = this.props;
      const {constructPraram,formatParam,natureParam} =this.state
      this.setState({
        propertyParam:qq
      })
      dispatch({
        type: 'example/fetchByProperty',
        payload: {
          ajax: 'lvxmXR',
          xmsx:qq,//建设属性
          xmyt:formatParam,//项目业态
          xmxz:natureParam,//项目性质
          jszt:constructPraram,//建设状态
        }
      })
    }

    //根据项目业态筛选
    getFormat = (e) =>{
      const condtion=e.join()
      const qq = encodeURI(condtion)
      this.setState({
        formatParam:qq
      })
      const {constructPraram,propertyParam,natureParam} =this.state
      const { dispatch } = this.props;
      dispatch({
        type: 'example/fetchByFormat',
        payload: {
          ajax: 'lvxmXR',
          xmyt:qq,//项目业态
          xmxz:natureParam,//项目性质
          jszt:constructPraram,//建设状态
          xmsx:propertyParam,//项目属性
        }
      })
    }
    //根据项目性质筛选
    getNature = (e) =>{
      const condtion=e.join()
      const qq = encodeURI(condtion)
      this.setState({
        natureParam:qq
      })
      const {constructPraram,propertyParam,formatParam} =this.state
      const { dispatch } = this.props;
      dispatch({
        type: 'example/fetchByNature',
        payload: {
          ajax: 'lvxmXR',
          xmxz:qq,//项目性质
          jszt:constructPraram,//建设状态
          xmsx:propertyParam,//项目属性
          xmyt:formatParam,//项目业态
        }
      })
    }

//显示项目数量图表
  viewItemTotal = (e) => {
      const tablename = e.target.innerHTML
      const isItemTotal = this.state
      this.setState({
        isItemTotal:true,
        isViewTotal:true,
        isViewNature:false,
        isViewCondition:false,
        isViewArea:false,
        tablename:tablename,
        tablenameTitle:'项目数量(个)'
      })
      const { dispatch } = this.props;
      dispatch({
        type: 'example/fetchByEcharts',
        payload: {
          ajax: 'lvxmEchart',
          type:'xms',//项目数
        }
      }).then(()=>{
          const { example: { echartsData } } = this.props
          const itemnum = new Array()
          const countyname = new Array()
          echartsData.data.rows.map(item =>{
            const itemshu = item.xms
            const itemcounty = item.qxmc
            itemnum.push(itemshu)
            countyname.push(itemcounty)
          })
           this.setState({
            itemxms:itemnum,
            itemcountyname:countyname
          })
      })
  }
  //项目面积图表
  viewItemArea = (e)=>{
    const tablename = e.target.innerHTML
      const isItemTotal = this.state
      this.setState({
        isItemTotal:true,
        isViewArea:true,
        isViewCondition:false,
        isViewNature:false,
        isViewTotal:false,
        tablename:tablename,
        tablenameTitle:'项目面积(亩)'
      })
      const { dispatch } = this.props;
      dispatch({
        type: 'example/fetchByEchartsArea',
        payload: {
          ajax: 'lvxmEchart',
          type:'xmmj',//项目面积
        }
      }).then(()=>{
          const { example: { echartsData } } = this.props
          const itemnum = new Array()
          const countyname = new Array()
          echartsData.data.rows.map(item =>{
            const itemshu = item.xmmj
            const itemcounty = item.qxmc
            itemnum.push(itemshu)
            countyname.push(itemcounty)
          })
           this.setState({
            itemxms:itemnum,
            itemcountyname:countyname
          })
      })
  }
  //项目建设状态图表
  viewItemCondition =(e) =>{
      const tablename = e.target.innerHTML
      const isItemTotal = this.state
      this.setState({
        isItemTotal:true,
        isViewArea:false,
        isViewCondition:true,
        isViewNature:false,
        isViewTotal:false,
        tablename:tablename,
        tablenameTitle:'项目数量(个)'
      })
      const { dispatch } = this.props;
      dispatch({
        type: 'example/fetchByEchartsCondition',
        payload: {
          ajax: 'lvxmEchart',
          type:'jszt',//建设状态
        }
      }).then(()=>{
          const { example: { echartsData } } = this.props
          const itemnum = new Array()
          const countyname = new Array()
          echartsData.data.rows.map(item =>{
            const itemshu = item.xms
            const itemcounty = item.jszt
            itemnum.push(itemshu)
            countyname.push(stateList[itemcounty - 1])
          })
           this.setState({
            itemxms:itemnum,
            itemcountyname:countyname
          })
      })
  }
  //项目属性图表
  viewItemNature =(e) => {
    const tablename = e.target.innerHTML
      const isItemTotal = this.state
      this.setState({
        isItemTotal:true,
        isViewArea:false,
        isViewCondition:false,
        isViewNature:true,
        isViewTotal:false,
        tablename:tablename,
        tablenameTitle:'项目数量(个)'
      })
      const { dispatch } = this.props;
      dispatch({
        type: 'example/fetchByEchartsNature',
        payload: {
          ajax: 'lvxmEchart',
          type:'xmsx',//项目属性
        }
      }).then(()=>{
          const { example: { echartsData } } = this.props
          const itemnum = new Array()
          const countyname = new Array()
          echartsData.data.rows.map(item =>{
            const itemshu = item.xms
            const itemcounty = item.xmsx
            itemnum.push(itemshu)
            countyname.push(itemPropertyList[itemcounty - 1])
          })
           this.setState({
            itemxms:itemnum,
            itemcountyname:countyname
          })
      })
  }
  //市县圆形图表
  getPieOption(item){
    const lengendData = ['项目前期','项目设计期','项目工程期','项目营销期','项目运营管理期']
    const pieData = [{
      name: lengendData[0],
      value: item.state === 1 ? item.statecount : 0
    },{
      name: lengendData[1],
      value: item.state === 2 ? item.statecount : 0
    },{
      name: lengendData[2],
      value: item.state === 3 ? item.statecount : 0
    },{
      name: lengendData[3],
      value: item.state === 4 ? item.statecount : 0
    },{
      name: lengendData[4],
      value: item.state === 5 ? item.statecount : 0
    }]
    const option={
       tooltip: {
          trigger: 'item',
          formatter: "{a} <br/>{b}: {c}"
       },
       legend: {
           orient : 'top',
           x : 'right',
           y : 'center',
           data:lengendData
       },
       color:['#e5954f', '#91f3f3','#6fa540','#729ef3','#a353d8'],
       series : [
        {
          name:'建设状态',
          type:'pie',
          position : 'left',
          radius : ['25%', '40%'],
          center: ['35%', '50%'],
          label: {
            normal: {
              formatter: '{b} {c}'
            }
          },
          data:pieData
        }
       ]
      }
    return option
  }
  getBarOption = (item) => {
    const lengendData = ['规划投资','实际投资']
    const xData = ['项目前期','项目设计期','项目工程期','项目营销期','项目运营管理期']
    const barData1 = [
      item.state === 1 ? item.buliddesign : 0,
      item.state === 2 ? item.buliddesign : 0,
      item.state === 3 ? item.buliddesign : 0,
      item.state === 4 ? item.buliddesign : 0,
      item.state === 5 ? item.buliddesign : 0,
    ]
    const barData2 = [
      item.state === 1 ? item.bulidactually : 0,
      item.state === 2 ? item.bulidactually : 0,
      item.state === 3 ? item.bulidactually : 0,
      item.state === 4 ? item.bulidactually : 0,
      item.state === 5 ? item.bulidactually : 0,
    ]
    const option = {
      tooltip: {
        trigger: 'axis'
      },
      calculable : true,
      legend: {
        data: lengendData
      },
      grid: {
        left: '15%',
      },
      xAxis : [
        {
          data : xData,
          axisLabel:{
            interval:0,//0：表示全部显示不间隔；auto:表示自动根据刻度个数和宽度自动设置间隔个数
          }
        }
      ],
      yAxis: [
        {type: 'value', name: '规划投资(万元)'},
        {type: 'value', name: '实际投资(万元)'}
      ],
      series: [
        {
          name: '规划投资',
          type: 'bar',
          data: barData1,
          itemStyle: {
            normal: {
              color: '#6fa540'
            }
          }
        },
        {
          name: '实际投资',
          type: 'bar',
          data: barData2,
          itemStyle: {
            normal: {
              color: '#a353d8'
            }
          }
        }
      ]
    }
    return option
  }
  //左侧统计图表
  getOption(){
    const {itemxms,itemcountyname,tablename,tablenameTitle} = this.state
     const option ={
       title:{text:tablename},
        tooltip:{},
        xAxis:{
          data:itemcountyname,
          axisLabel:{
            interval:0,//0：表示全部显示不间隔；auto:表示自动根据刻度个数和宽度自动设置间隔个数
          },
        },
        grid: {
         left: '10%',
        },
        yAxis:[
          {
            type: 'value', name: tablenameTitle
          }
        ],
        series:[{
          name:tablenameTitle,
          type:'bar',
          data:itemxms,
          barWidth:25
        }]
    }
    return option
  }

//表格日期操作
riqiSelect(e){
  console.log(e)
  const {riqiColor} =this.state
  this.setState({
    riqiColor:e
  })
}

  //表格操作
  tableAction(e){
    console.log(e)
  }
  handleShowMenu = () => {
    const { isShowMenu } = this.state;
    this.setState({
      isShowMenu: !isShowMenu
    })
  }

  handleConstruct = () => {
    const { isConstruct } = this.state;
    this.setState({
      isConstruct: !isConstruct,
      isStatus:false,
      isProp:false,
      isProject:false
    })
  }
  handleProp = () => {
    const { isProp } = this.state;
    this.setState({
      isProp: !isProp,
      isProject:false,
      isConstruct:false,
      isStatus:false,
    })
  }
  handleStatus = () => {
    const { isStatus } = this.state;
    this.setState({
      isStatus: !isStatus,
      isProp:false,
      isProject:false,
      isConstruct:false
    })
  }
  handleProject = () => {
    const { isProject } = this.state;
    this.setState({
      isProject: !isProject,
      isStatus:false,
      isProp:false,
      isConstruct:false
    })
  }
  handleInfo = (item,e) => {
    let param = {
      longitude: item.position.longitude,
      latitude: item.position.latitude
    }
    const title = item.title;
    const projectList = item.list;
    this.setState({
      position:param,
      visible:true,
      title:title,
      projectList:projectList,
      isImg:false,
      selectedImg:title,
      offset:[-1,-12],
      selectedQxbm: item.qxbm
    })
  }
  goBack(e){
    window.location.reload();
  }
  //显示项目详情
  viewItemDetail= (item) => {
    const clientHeight = document.querySelector('body').offsetHeight - 50
    const xmmc = this.state.title
    const xmmctwo = xmmc.trim()
    const xmmcCode = encodeURI(xmmctwo)
    const { dispatch } = this.props;
    dispatch({
      type: 'example/fetchByItemsDetails',
      payload: {
        ajax: 'xmDetail',
        id:item.id,//项目id
      }
    }).then(()=>{
      this.setState({
        isShowItemDetail:true,
        windowHeight:clientHeight
      })
      const { example: { ItemDetails } } = this.props
    })
  }
  //关闭项目详情
  closeItemDetail(e){
    this.setState({
      isShowItemDetail:false,
      windowHeight:0
    })
  }
//查看项目建设情况
viewItemDevelopment = (item,e) => {
  console.log(item)
  const clientHeight = document.querySelector('body').offsetHeight-40
  this.setState({
    isShowItemDevelopment:true,
    windowHeight:clientHeight,
  })
  const itemId = item.id
  const { dispatch } = this.props;
  dispatch({
    type: 'example/fetchDevelopmentSituation',
    payload: {
      ajax: 'lvxmKfqk',
      id:itemId,//项目id
    }
  }).then(() => {
    const { example: {DevelopmentSituation} } = this.props
    if(DevelopmentSituation[0].id) {
      const date = DevelopmentSituation[0].listdate.length && DevelopmentSituation[0].listdate[0]
      if(date) {
        dispatch({
          type: 'example/fetchImg',
          payload: {
            ajax: 'lvxmXmtp',
            date: date
          }
        })
      }
    }
  })
  dispatch({
    type: 'example/fetchPage',
    payload: {
      ajax: 'lvxmXmkfqk',
      pagenum: 1,
      pagesize: 10,
      id: itemId
    }
  })
  
}
//关闭项目建设情况
closeItemDevelopment(){
  this.setState({
    isShowItemDevelopment:false,
    windowHeight:0
  })
}
handleNext = (item,e) => {
  const { dispatch } = this.props;
  const { selectedQxbm } = this.state
  dispatch({
    type: 'example/fetchNext',
    payload: {
      ajax: 'lvxmQX',
      qxbm: item.qxbm
    }
  }).then(() => {
    const { example: { subData }} = this.props;
    let subItemList = subData.rows && subData.rows.map(item => {
       return {
        position: {
          longitude: item.position.split(',')[0],
          latitude: item.position.split(',')[1],
        },
        label: {
          content: item.title
        },
        title: item.title,
        ztz: item.ztz,
        list: [{
          id: item.id,
          itemnature: item.itemnature ? item.itemnature : '',
          itemtype:item.itemtype && item.itemtype.split(',').map(k => {
            return itemTypeList[k-1]
          }),
          itmeproperty:item.itmeproperty && item.itmeproperty.split(',').map(i => {
            return itemPropertyList[i - 1]
          }),
          itemformat:item.itemformat && item.itemformat.split(',').map(n => {
            return formatList[n - 1]
          }),
          planningland:item.planningland,
          importantitem:item.importantitem && item.importantitem.split(',').map(m => {
            return imItemList[m - 1]
          }),
          itemstate:stateList[item.itemstate - 1]
        },
        {
          completename:'项目类型',
          completeNum:12,
          completedesign:8,
          completeactually:3
        },
        {
          bulidname:'项目业态',
          bulidNum:3,
          buliddesign:10,
          bulidactually:3
        },
        {
          planningname:'项目建设状态',
          planningNum:2,
          planningdesign:3,
          planningactually:3
        }]
      }
    })
    const _this = this
    const xian = _this.state.title
    window.AMap.plugin('AMap.DistrictSearch', () => {
      _this.district = new window.AMap.DistrictSearch({
        extensions: 'all',
        // 设置查询行政区级别为 区
        level: 'district'
      });
      _this.district.search(xian, function(status, result) {
     })
    })
    this.setState({
      subPos: subItemList,
      zoom: 12,
      visible: false,
      isSub: true,
      clickMark : false,
      center: subItemList[0].position,
      size:{
        width: 280,
        height: 345,
      }
    })
    this.subMarkersEvents = {
      click: (MapsOption) => {
        let p = {
          longitude: MapsOption.lnglat.lng,
          latitude: MapsOption.lnglat.lat
        }
        let marker = MapsOption.target
         const extData = marker.getExtData();
         const title = extData.title
         const projectList = extData.list
        this.setState({
          position:p,
          visible: true,
          title: title,
          projectList: projectList,
          selectedImg: title,
          offset:[0,0]
        })
        //renderMarkerLayout()
      },
    }
    this.windowEvents = {
      created: (iw) => {
        console.log(iw)
      },
      open: () => {console.log('InfoWindow opened')},
      close: () => {
        console.log('InfoWindow closed')
        this.setState({
          visible: false,
          selectedImg:''
        })
      },
      change: () => {console.log('InfoWindow prop changed')},
    }
  })
}
  handleCity(item,type,e) {
    const _this = this
    const xian = _this.state.title
    let totalItem,finishItem,bulidingItem,planningItem
    //全部项目
    if(type ==1 ){
      totalItem = item.subList
      if(totalItem !== 0){
        console.log(totalItem)
        var secondFloorMarkers = new Array()
        totalItem.map(item =>{
        const qq = {
          position: {
            longitude: item.position.longitude,
            latitude: item.position.latitude
          },
          label: {
            content: '项目1'
          },
          title: item.title,
          ztz:item.ztz,
          list: [{
            id: item.list[0].id,
            itemnature: item.list[0].itemnature,
            itemtype:item.list[0].itemtype,
            itmeproperty:item.list[0].itmeproperty,
            itemformat:item.list[0].itemformat,
            planningland:item.list[0].planningland,
            importantitem:item.list[0].importantitem,
            itemstate:item.list[0].itemstate
          },
          {
            completename:'项目类型',
            completeNum:12,
            completedesign:8,
            completeactually:3
          },
          {
            bulidname:'项目业态',
            bulidNum:3,
            buliddesign:10,
            bulidactually:3
          },
          {
            planningname:'项目建设状态',
            planningNum:2,
            planningdesign:3,
            planningactually:3
          }]
        }
        secondFloorMarkers.push(qq)
      })
      }else{
        console.log('没有数据')
        return
      }
    }
    //已完成
    if(type == 2){
      finishItem = item.subList.filter(sub => {
        return sub.jszt == '已完成'
      })
      if(finishItem.length !==0){
        var secondFloorMarkers = new Array()
        finishItem.map(item =>{
        const qq = {
          position: {
            longitude: item.position.longitude,
            latitude: item.position.latitude
          },
          label: {
            content: '项目1'
          },
          title: item.title,
          ztz:item.ztz,
          list: [{
            id: item.list[0].id,
            itemnature: item.list[0].itemnature,
            itemtype:item.list[0].itemtype,
            itmeproperty:item.list[0].itmeproperty,
            itemformat:item.list[0].itemformat,
            planningland:item.list[0].planningland,
            importantitem:item.list[0].importantitem,
            itemstate:item.list[0].itemstate
          },
          {
            completename:'项目类型',
            completeNum:12,
            completedesign:8,
            completeactually:3
          },
          {
            bulidname:'项目业态',
            bulidNum:3,
            buliddesign:10,
            bulidactually:3
          },
          {
            planningname:'项目建设状态',
            planningNum:2,
            planningdesign:3,
            planningactually:3
          }]
        }
        secondFloorMarkers.push(qq)
      })
      }else{
        console.log('没有数据')
        return
      }
    }
    //建设数量
    if(type==3){
      bulidingItem = item.subList.filter(sub => {
        return sub.jszt == '建设中'
      })
      if(bulidingItem.length !== 0){
        var secondFloorMarkers = new Array()
        bulidingItem.map(item =>{
        const qq = {
          position: {
            longitude: item.position.longitude,
            latitude: item.position.latitude
          },
          label: {
            content: '项目1'
          },
          title: item.title,
          ztz:item.ztz,
          list: [{
            id: item.list[0].id,
            itemnature: item.list[0].itemnature,
            itemtype:item.list[0].itemtype,
            itmeproperty:item.list[0].itmeproperty,
            itemformat:item.list[0].itemformat,
            planningland:item.list[0].planningland,
            importantitem:item.list[0].importantitem,
            itemstate:item.list[0].itemstate
          },
          {
            completename:'项目类型',
            completeNum:12,
            completedesign:8,
            completeactually:3
          },
          {
            bulidname:'项目业态',
            bulidNum:3,
            buliddesign:10,
            bulidactually:3
          },
          {
            planningname:'项目建设状态',
            planningNum:2,
            planningdesign:3,
            planningactually:3
          }]
        }
        secondFloorMarkers.push(qq)
      })
      }else{
        console.log('没有数据')
        return
      }
    }
    if(type == 4){
      planningItem = item.subList.filter(sub =>{
        return sub.jszt == '规划中'
      })
      if(planningItem.length!==0){
        var secondFloorMarkers = new Array()
        planningItem.map(item =>{
        const qq = {
          position: {
            longitude: item.position.longitude,
            latitude: item.position.latitude
          },
          label: {
            content: '项目1'
          },
          title: item.title,
          ztz:item.ztz,
          list: [{
            id: item.list[0].id,
            itemnature: item.list[0].itemnature,
            itemtype:item.list[0].itemtype,
            itmeproperty:item.list[0].itmeproperty,
            itemformat:item.list[0].itemformat,
            planningland:item.list[0].planningland,
            importantitem:item.list[0].importantitem,
            itemstate:item.list[0].itemstate
          },
          {
            completename:'项目类型',
            completeNum:12,
            completedesign:8,
            completeactually:3
          },
          {
            bulidname:'项目业态',
            bulidNum:3,
            buliddesign:10,
            bulidactually:3
          },
          {
            planningname:'项目建设状态',
            planningNum:2,
            planningdesign:3,
            planningactually:3
          }]
        }
        secondFloorMarkers.push(qq)
      })
      }else{
        console.log('没有数据')
        return
      }
    }

    window.AMap.plugin('AMap.DistrictSearch', () => {
      _this.district = new window.AMap.DistrictSearch({
        extensions: 'all',
        // 设置查询行政区级别为 区
        level: 'district'
      });
      _this.district.search(xian, function(status, result) {
     })
    })

  const subPos = secondFloorMarkers
    this.setState({
      subPos: subPos,
      zoom: 12,
      visible: false,
      isSub: true,
      clickMark : false,
      center: subPos[0].position,
      size:{
        width: 280,
        height: 345,
      }
    })
    this.subMarkersEvents = {
      click: (MapsOption) => {
        let p = {
          longitude: MapsOption.lnglat.lng,
          latitude: MapsOption.lnglat.lat
        }
        let marker = MapsOption.target
         const extData = marker.getExtData();
         const title = extData.title
         const projectList = extData.list
        this.setState({
          position:p,
          visible: true,
          title: title,
          projectList: projectList,
          selectedImg: title,
          offset:[0,-12]
        })
        //renderMarkerLayout()
      },
    }
    this.windowEvents = {
      created: (iw) => {
        console.log(iw)
      },
      open: () => {console.log('InfoWindow opened')},
      close: () => {
        console.log('InfoWindow closed')
        this.setState({
          visible: false,
          selectedImg:''
        })
      },
      change: () => {console.log('InfoWindow prop changed')},
    }
  }
  initPage = (map,DistrictExplorer) => {
    const district = new DistrictExplorer({
      map: map
    })
    const that = this
    const adcode = '532500'
    district.loadAreaNode(adcode, function(error, areacode) {
      if(error) {
        return
      }
      that.renderAreaNode(district, areacode)
    })
  }
  renderAreaNode(districtExplorer, areaNode) {
    districtExplorer.clearFeaturePolygons();
    var colors = ["#bfc7d2", "#d9dee4", "#e9eaef", "#e7eaef", "#e7e8ed", "#f8f9fb", "#f2f3f5", "#cdd1dd", "#bfc7d2", "#dcdfe8", "#ced5dd", "#e6e9ee", "#ecedf1"];
    //绘制子级区划ss
   districtExplorer.renderSubFeatures(areaNode, function(feature, i) {
        var fillColor = colors[i % colors.length];
        var strokeColor = colors[colors.length - 1 - i % colors.length];
        return {
          cursor: 'default',
          bubble: true,
          strokeColor: strokeColor, //线颜色
          strokeOpacity: 1, //线透明度
          strokeWeight: 1, //线宽
          fillColor: fillColor, //填充色
          fillOpacity: 0.8, //填充透明度
        };
    });
     //绘制父级区划，仅用黑色描边
   districtExplorer.renderParentFeature(areaNode, {
        cursor: 'default',
        bubble: true,
        strokeColor: '#c1cad3', //线颜色
        fillColor: null,
        strokeWeight: 3, //线宽
    });
  //  console.log(districtExplorer.getAllFeaturePolygons())
  }
  render() {
    const _this = this;
    this.district  = null
    this.amapEvents = {
      created: (mapInstance) => {
        console.log('高德地图 Map 实例创建成功；如果你要亲自对实例进行操作，可以从这里开始。比如：');
        console.log(mapInstance.getZoom());
        window.AMap.plugin('AMap.DistrictSearch',() => {
          _this.district = new window.AMap.DistrictSearch({
            extensions: 'all',
            // 设置查询行政区级别为 区
            level: 'city'
          });
          _this.district.search('红河哈尼族彝族自治州', function(status, result) {
            var bounds = result.districtList[0].boundaries
            var polygons = []
            if (bounds) {
              for (var i = 0, l = bounds.length; i < l; i++) {
               polygons.push({
                 path: bounds[i]
               })
             }
             _this.setState({
               list: polygons
             })
           }
         })
        })
        window.AMapUI.loadUI(['geo/DistrictExplorer'], (DistrictExplorer) => {
          this.initPage(mapInstance, DistrictExplorer)
        })
      },
      click:(mapInstance) => {
        this.setState({
          isConstruct: false,
          isStatus:false,
          isProp:false,
          isProject:false,
          isShowMenu:false
        })
      }
    };
    this.pevents = {
      created: (ins) => {
        ins.Pg.map.setFitView()
        console.log(ins.Pg)
      }
    }
    this.markersEvents = {
      created: (instance) => {
      },
      click: (MapsOption) => {
        let marker = MapsOption.target
        const extData = marker.getExtData();
        if(!extData.label.noxms){
          return
        }
        let param = {
          longitude: extData.position.longitude,
          latitude: extData.position.latitude
        }
        const title = extData.title
        const projectList = extData.list
        this.setState({
          position: param,
          visible: true,
          title: title,
          projectList: projectList,
          selectedImg: title,
          offset:[3,-12],
          selectedQxbm: extData.qxbm
        })
      }
    }

    this.windowEvents = {
      created: (iw) => {console.log(iw)},
      open: () => {console.log('InfoWindow opened')},
      close: () => {
        console.log('InfoWindow closed')
        this.setState({
          visible: false,
          selectedImg: ''
        })
      },
      change: () => {console.log('InfoWindow prop changed')},
    }
    const { mapKey, riqiColor,list, title, projectList, visible, subPos, zoom, isSub, center,selectedImg,
      windowHeight,isShowItemDetail,isShowItemDevelopment,isConstruct, isShowMenu,isProject,isProp, isStatus ,isItemTotal,isViewArea,isViewTotal,isViewCondition,isViewNature,tablename, construct
    } = this.state;
    const pList = list && list.map(item => {
      return <Polygon path={item.path} events={this.pevents} style={{fillOpacity: 0}}  />
    })
    let position = new Array();
    const { example: { poslist,tableList, total ,ItemDetails,DevelopmentSituation} } = this.props
    if(JSON.stringify(total) != "{}"){
        total.data.rows.map(item =>{
          if("list" in item){
            const qxbm = item.qxbm;
            const obj = {
            position: {
              longitude: item.position.longitude,
              latitude: item.position.latitude
            },
            label: {
              noxms : true,
              content: '项目数量：',
            },
            title: item.title,
            xmsl:item.xmsl,
            list: item.list,
            qxbm: item.qxbm
          };
          const subPos = new Array();
          position.push(obj);
          }else{
            const qxbm = item.qxbm;
            const obj = {
            position: {
              longitude: item.position.longitude,
              latitude: item.position.latitude
            },
            label: {
              noxms : false,
              content: '项目数量：',
            },
            title: item.title,
            xmsl:item.xmsl,
            list: [{
              id: 1,
              itemname: '项目数量',
              itemcount: 0,
              itemdesign:0,
              itemactually:0,

            },
            {
              completename:'完工数',
              completeNum:0,
              completedesign:0,
              completeactually:0
            },
            {
              bulidname:'建设数量',
              bulidNum:0,
              buliddesign:0,
              bulidactually:0
            },
            {
              planningname:'计划数量',
              planningNum:0,
              planningdesign:0,
              planningactually:0
            }],
            list:list
          };
          const subPos = new Array();
          position.push(obj);
            }
      })
    }

    const infoList = projectList && projectList.map(item => {
      const firstFloor = <div>
          <div className={styles.pietable}><ReactEcharts option={this.getPieOption(item)}  barWidth="30" style={{width: '100%', height: '200px'}}/></div>
          <div className={styles.barTable}>
            <ReactEcharts option={this.getBarOption(item)} style={{width: '100%', height: '200px'}} />
          </div>
        </div>
      const secondFloor = (
      <ul key={projectList[0].id}>
        <li>项目属性：{projectList[0].itemnature}</li>
        <li>项目类型：{projectList[0].itemtype}</li>
        <li>项目性质：{projectList[0].itmeproperty}</li>
        <li>项目业态：{projectList[0].itemformat}</li>
        <li>规划用地：{projectList[0].planningland}亩</li>
        <li>重点项目性质：{projectList[0].importantitem}</li>
        <li>项目建设状态：{projectList[0].itemstate}</li>
        <div className={styles.InfoWindowbtn}>
          <button onClick={() =>this.viewItemDetail(item)}>查看项目详情</button></div>
        <div className={styles.InfoWindowview} onClick={(e) => this.viewItemDevelopment(item,e)}>查看项目建设情况</div>
      </ul>);
      return isSub ? (secondFloor) : (firstFloor)
    })

    // 一级marker
    const markerList = position && position.map((item) => {
      return <Marker position={item.position} events={this.markersEvents} extData={{
        title: item.title,
        label:item.label,
        list: item.list,
        position:item.position,
        qxbm: item.qxbm
      }}>

      {item.label.noxms?(<div className={styles.title} onClick={(e) => this.handleNext(item,e)}><div className={styles.tittlename}>{item.title}</div><div>项目数量{item.xmsl}</div></div>):(<div className={styles.title}><div className={styles.tittlename}>{item.title}</div><div>项目数量{item.xmsl}</div></div>)}
        <div className={styles.triangle_bottom}></div>
        {selectedImg == item.title ? (<img className={styles.imgstyless} src={require('../assets/icon2.png')} alt="" />) : (<img className={styles.imgstyle} src={require('../assets/dot03.png')} alt="" />)}
      </Marker>
    })
    // 二级marker
    const submarkerList = subPos.map((item) => {
      return <Marker position={item.position} events={this.subMarkersEvents} extData={{
        title: item.title,
        list: item.list
      }}>
        <div className={styles.secondtitle} onClick={(e) => this.handleInfo(item,e)}><div className={styles.secondtittlename}>{item.title}</div><div className={styles.totalInvestment}>总投资：{item.ztz}(万元)</div></div><div className={styles.triangle_bottom}></div>
        {selectedImg == item.title ? (<img className={styles.imgstyless} src={require('../assets/icon2.png')} alt="" />) : (<img className={styles.imgstyle} src={require('../assets/dot03.png')} alt="" />)}
      </Marker>
    })

  const columns = [{
  title: '建设期',
  dataIndex: 'rq',

}, {
  title: '建设情况说明',
  dataIndex: 'sm',
}, {
  title: '现场图片数量',
  dataIndex: 'tpsl',
},
{
  title:'填报时间',
  dataIndex:'tbsj',
},
{
  title:'填报人',
  dataIndex:'sjtby'
},
{
title: '操作',
render:  (record) => <a href="javascript:;" onClick={() => this.tableAction(record)}>查看现场情况</a>,
}
  
];

// const imageArray = [
//   {src:require('../assets/img.png')},
//   {src:require('../assets/img.png')},
//   {src:require('../assets/img.png')},
// ];
const labelName = DevelopmentSituation && DevelopmentSituation.map(item => {
  return <div className={styles.selectImgBtn}>{item.dw} </div>
})
const thatitemname = DevelopmentSituation && DevelopmentSituation.map(item => {
  return item.XMMC
})
const DevelopmentSituationName = DevelopmentSituation && DevelopmentSituation.map(item => {
  const Developmentxmmc = <div>{item.XMMC} </div>
  return Developmentxmmc
})
const DevelopmentSituationTime = DevelopmentSituation[0] && DevelopmentSituation[0].listdate.map(item => {
  console.log(item)
  return <div className={riqiColor == item.RQ ?(styles.timeDotSelect):(styles.timeDot) } onClick={(e) => this.riqiSelect(item)}>{item} </div>
})
    return (
      <div className={styles.container}>
      <div className={styles.itemDetailCont}>
        {
          isShowItemDevelopment?(<div style={{height:windowHeight}} className={styles.itemdetailDiv}>
          <div className={styles.closeBtn} onClick={(e) => this.closeItemDevelopment(e)}>X</div>
          <div className={styles.itemDevelopment}>
            <div className={styles.itemTitleInfo}>
              <div className={styles.itemTitleInfoone}>项目名称<div>{thatitemname[0]}</div></div>
              <div className={styles.itemTitleInfotwo}>现场图片数量<div>22</div></div>
            </div>
            <div className={styles.topDevelopment}>
              <div className={styles.topimage}>

                <Carousel autoplay>
                  <div className={styles.sliderImg}><img src="http://image.tuoketech.com/20161030191519234800_1.jpg" /></div>
                  <div className={styles.sliderImg}><img src="http://image.tuoketech.com/1293464_20170512190501284020_1.jpg" /></div>
                  <div className={styles.sliderImg}><img src="http://image.tuoketech.com/122260_20130817212106986200_1.jpg" /></div>
                </Carousel>
              </div>
              <div className={styles.selectimg}>
                <div>图片标签选择</div>
                {labelName}
              </div>
              <div className={styles.selectTime}>
                <div className={styles.timeDotTitle}>日期选择</div>
                <div className={styles.timeCont}>
                  {DevelopmentSituationTime}
                </div>
              </div>
            </div>
            <h3 className={styles.topTitle}>项目建设情况</h3>
            <Table columns={columns} dataSource={tableList}  pagination={{ pageSize: 3 }} />
          </div>
        </div>):(null)}
      </div>

      <div className={styles.itemDetailCont}>
        {
          isShowItemDetail?(<div style={{height:windowHeight}} className={styles.itemdetailDiv}>
            <div className={styles.closeBtn} onClick={(e) => this.closeItemDetail(e)}>X</div>
            <div className={styles.detailInfoCont}>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  项目名称
                </div>
                <div className={styles.detailRight}>
                  {ItemDetails.rows && ItemDetails.rows.XMM}
                </div>
              </div>

              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  项目地址
                </div>
                <div className={styles.detailRight}>
                  {ItemDetails.rows && ItemDetails.rows.XMDZ}
                </div>
              </div>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  项目概况
                </div>
                <div className={styles.detailRight}>
                  {ItemDetails.rows && ItemDetails.rows.XMGK}
                </div>
              </div>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  四至范围
                </div>
                <div className={styles.detailRight}>
                  {ItemDetails.rows && ItemDetails.rows.SZFW}
                </div>
              </div>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  项目选址
                </div>
                <div className={styles.detailRight}>
                  <div className={styles.itemdiv}>{ItemDetails.rows && projectPosition[ItemDetails.rows.XMXZLX - 1]}</div>
                  <div className={styles.itemdiv}>
                  <span>项目属性：</span>{ItemDetails.rows && ItemDetails.rows.XMSX.split(',').map(k => {
                    return itemPropertyList[k - 1]
                  }).join(',')}</div>
                  <div className={styles.itemdiv}>
                    <span>项目类型：</span>{ItemDetails.rows && ItemDetails.rows.XMLX.split(',').map(i => {
                      return itemTypeList[i - 1]
                    }).join(',')}
                  </div>
                  <div className={styles.itemdiv}>
                    <span>项目性质：</span>{ItemDetails.rows && ItemDetails.rows.XMXZ.split(',').map(n => {
                      return imItemList[n - 1]
                    }).join(',')}
                  </div>
                </div>
              </div>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  总投资(万元)
                </div>
                <div className={styles.detailRight}>
                  <div className={styles.itemdiv}>{ItemDetails.rows && ItemDetails.rows.ZTZ}</div>
                  <div className={styles.itemdiv}>
                    <span>项目业态：</span>{ItemDetails.rows && ItemDetails.rows.XMYT.split(',').map(m => {
                      return formatList[m - 1]
                    }).join(',')}
                  </div>
                </div>
              </div>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  规划用地(亩)
                </div>
                <div className={styles.detailRight}>
                <div className={styles.itemdiv}>{ItemDetails.rows && ItemDetails.rows.GHYD}</div>
                </div>
              </div>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  重点项目性质及文号
                </div>
                <div className={styles.detailRight}>
                  <div className={styles.itemdiv}>国家发改委</div>
                  <div className={styles.itemdiv}><span>文号：</span>{ItemDetails.rows && ItemDetails.rows.ZDXMXZJWH}</div>
                </div>
              </div>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  开工时间
                </div>
                <div className={styles.detailRight}>
                  <div className={styles.itemdiv}>{ItemDetails.rows && ItemDetails.rows.KGRQ}</div>
                  <div className={styles.itemdiv}><span>建设周期：</span>{ItemDetails.rows && ItemDetails.rows.JSZQKSRQ} - {ItemDetails.rows && ItemDetails.rows.JSZQJSRQ}</div>
                  <div className={styles.itemdiv}><span>实际投资(万元)：</span>{ItemDetails.rows && ItemDetails.rows.SJLJTZ}</div>
                  <div className={styles.itemdiv}><span>上报方式：</span>sss</div>
                  <div className={styles.itemdiv}><span>上报审核：</span>sss</div>
                </div>
              </div>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  项目管理单位
                </div>
                <div className={styles.detailRight}>
                  <div className={styles.itemdiv}><span>项目管理单位名称：</span>{ItemDetails.rows && ItemDetails.rows.xmzgdw}</div>
                  <div className={styles.itemdiv}><span>详细地址：</span>{ItemDetails.rows && ItemDetails.rows.xmm}</div>
                  <div className={styles.itemdiv}><span>联系电话：</span>{ItemDetails.rows && ItemDetails.rows.xmzrdh}</div>
                  <div className={styles.itemdiv}><span>联系人：</span>{ItemDetails.rows && ItemDetails.rows.xmfzr}</div>
                  <div className={styles.itemdiv}><span>详情：</span>。。。。</div>
                </div>
              </div>
              <div className={styles.itemDetailList}>
                <div className={styles.detailLeft}>
                  投资单位信息
                </div>
                <div className={styles.detailRight}>
                  <div className={styles.itemdiv}><span>投资单位名称：</span>{ItemDetails.rows && ItemDetails.rows.tzdwmc}</div>
                  <div className={styles.itemdiv}><span>性质：</span>{ItemDetails.rows && ItemDetails.rows.tzxz}</div>
                  <div className={styles.itemdiv}><span>投资比例：</span>{ItemDetails.rows && ItemDetails.rows.tzbl}</div>
                  <div className={styles.itemdiv}><span>备注：</span>{ItemDetails.rows && ItemDetails.rows.bz}</div>
                  <div className={styles.itemdiv}><span>投资单位详情：</span>mmmm</div>
                  <div className={styles.itemdiv}><span>资本金到位情况</span>{ItemDetails.rows && ItemDetails.rows.tzdwqk}</div>
                </div>
              </div>
            </div>
            </div>):(null)
        }
      </div>
        <div className={styles.leftmenu}>
        <div className={styles.lefticon} onClick={this.handleShowMenu}></div>
        <div className={styles.menulist}>
            {
              isShowMenu ?(<div><div className={isViewTotal?(styles.clickcolor):(null)} onClick={this.viewItemTotal}>总项目数统计图</div>
                <div className={isViewArea?(styles.clickcolor):(null)}  onClick={this.viewItemArea}>总面积统计图</div>
                <div className={isViewCondition?(styles.clickcolor):(null)} onClick={this.viewItemCondition}>建设状态统计图</div>
                <div className={isViewNature?(styles.clickcolor):(null)} onClick={this.viewItemNature}>项目属性统计图</div>
                </div>
              ): null
            }
       </div>
       <div className={styles.leftChart}>
         {
           isShowMenu && isItemTotal ? (
           <div>
             <ReactEcharts option={this.getOption()} style={{width: '100%', height: '300px'}} barWidth="30"/>
           </div>
           ):null
          }
       </div>

      </div>
      <div className={styles.topfixed}>
          <div className={styles.rightItem}>
            <div className={styles.itemTitle} onClick={this.handleConstruct}>建设状态选择</div>
            <div className={isConstruct?(styles.thatStyle):(styles.checkWrap)} >
                <Checkbox.Group style={{width: '100%'}} onChange={this.getConstruct} defaultValue={construct}>
                <div className={styles.checkItem}><Checkbox value="1">项目前期</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="2">项目设计期</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="3">项目工程期</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="4">项目营销期</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="5">项目运营管理期</Checkbox></div>
              </Checkbox.Group>
            </div>
          </div>
          <div className={styles.rightItem}>
            <div className={styles.itemTitle} onClick={this.handleProp}>项目属性选择</div>
            <div className={isProp?(styles.checkWrapStyle):(styles.checkWrap)}>
              <Checkbox.Group style={{width: '100%'}} onChange={this.getProperty}>
                <div className={styles.checkItem}><Checkbox value="1">观光</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="2">度假</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="3">专项</Checkbox></div>
              </Checkbox.Group>
            </div>
          </div>
          <div className={styles.rightItem}>
            <div className={styles.itemTitle} onClick={this.handleStatus}>项目业态选择</div>
            <div className={isStatus?(styles.thatStyle):(styles.checkWrap)}>
              <Checkbox.Group style={{width: '100%'}} onChange={this.getFormat}>
                <div className={styles.checkItem}><Checkbox value="1">文化旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="2">生态旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="3">海洋旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="4">医疗健康旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="5">乡村旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="6">商务旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="7">红色旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="8">体育旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="9">工业旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="10">森林旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="11">游轮游艇旅游</Checkbox></div>
                <div className={styles.checkItem}><Checkbox value="12">航空旅游</Checkbox></div>
              </Checkbox.Group>
            </div>
          </div>
          <div className={styles.rightItem}>
            <div className={styles.itemTitle} onClick={this.handleProject} >项目性质选择</div>
            <div className={isProject?(styles.thatStyle):(styles.checkWrap)}>
             <Checkbox.Group style={{width: '100%'}} onChange={this.getNature}>
                 <div className={styles.checkItem}><Checkbox value="1">文化旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="2">生态旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="3">海洋旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="4">医疗健康旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="5">乡村旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="6">商务旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="7">红色旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="8">体育旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="9">工业旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="10">森林旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="11">游轮游艇旅游</Checkbox></div>
                 <div className={styles.checkItem}><Checkbox value="12">航空旅游</Checkbox></div>
              </Checkbox.Group>
            </div>
          </div>
      </div>

        <Map amapkey={mapKey} events={this.amapEvents} zoom={zoom} center={center} useAMapUI>
          <InfoWindow
              position={this.state.position}
              visible={visible}
              size={this.state.size}
              offset={this.state.offset}
              events={this.windowEvents}
              closeWhenClickMap={true}
          >
            <h4 className={styles.infoTitle}>{title}</h4>
            <div className={`${styles.projectList} ${styles.clearfix}`}>
              {infoList[0]}
            </div>
          </InfoWindow>
          {isSub ? (<div className={styles.goback} onClick={(e) => this.goBack()}>返回</div>):(<div></div>)}
          {
            !isSub ? markerList : submarkerList
          }
            {pList}
        </Map>
      </div>
    )
  }
}
